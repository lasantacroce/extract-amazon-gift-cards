# Extract Amazon Gift Card Codes (MATLAB function)
Credit: Lindsay Santacroce (contact: lindsayas22@gmail.com)

## Description & Background
This function allows users to extract Amazon gift card codes from a large amount of emails using .html files of the email chain(s). I created this function specifically because my lab pays participants in Amazon gift cards, and so $5 gift cards are bought in bulk by the hundreds. Historically, it was someone's job to go through the emails sent from Amazon and copy/paste each gift card's claim code into an Excel sheet. Not only is this time-consuming, but it also leaves room for human error. Thus, I created this function to do the job for me!

## Interface
- Input: .html file(s) of the email chain(s) containing the Amazon gift card codes
- Output: A .csv file with all of the gift card claim codes from the email(s)

## Instructions
1. Download all files in the 'DOWNLOADME' folder
2. Add that function folder to MATLAB path
3. Download .html files of the email(s) sent from Amazon:
    
      3.1. Navigate to the email chain that Amazon sends containing the gift card claim codes

      3.2. Open the email chain in a new window and expand the email chain

      3.3. Click Ctrl+S to save the email chain as an .html file
        
      3.4. Amazon sends max 100 emails per chain, so repeat this step for as many email chains that contain your gift card codes
4. Type "ExtractAmazonCodes" into the MATLAB Command Window and hit Enter
5. Select all the .html files you download (Ctrl+click to select more than one) and click 'Continue'
6. When the function is complete, a .csv file will appear in the same folder as your .html files that will contain all Amazon gift card claim codes!

## Visual
![](ExtractAmazonCodes_visual.png)

## Support
If you have any questions/errors/comments/ideas for additions, contact me at lindsayas22@gmail.com and include "Extract Amazon Gift Cards" in the subject line.
