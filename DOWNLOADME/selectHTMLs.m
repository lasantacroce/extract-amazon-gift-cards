%% Select html files box
%Credit: Lindsay Santacroce (contact: lindsayas22@gmail.com)

function htmlFiles = selectHTMLs

%% Box
%Set up box
f = uifigure('Name', 'File Information', ... 
    'NumberTitle', 'off', ...
    'MenuBar', 'none', ...
    'Position', [500 500 400 187] ...
    );    
fTitle = uicontrol(f, ... 
    'Style', 'Text', ...
    'Position', [0 150 400 30], ...
    'FontSize', 16, ...
    'HorizontalAlignment', 'center', ...
    'String', 'Target Spell Check File Information' ...  
    );

%Select files
    %panel
    pH = uipanel(f, ... 
        'Units', 'pixels', ...
        'Position', [30 52 340 90] ...
        ); 
    %question
    tH = uicontrol(pH, ... 
        'Style', 'text', ...
        'Position', [10 32 330 50], ...
        'FontSize', 12, ...
        'HorizontalAlignment', 'left', ...
        'String', {'Select the email .html file(s) that contain the Amazon gift card codes'} ...
        );
    %get file button
    bH = uibutton(pH, ...
        'Position', [15 12 80 22], ...
        'Text', 'Select File(s)' ...
        );
        %callback function
        bH.ButtonPushedFcn = @getHTMLs;
    %number of files selected
    nF = uicontrol(pH, ... 
        'Style', 'text', ...
        'Position', [100 8 200 22], ...
        'HorizontalAlignment', 'left', ...
        'String', '0 files selected' ...
        );

%Continue
    %button    
    bEnter = uibutton(f, ...
            'Position', [240 15 90 25], ...
            'Text', 'Continue' ...
            );    
    %callback function
    bEnter.ButtonPushedFcn = @continueToExtraction;

%user data setup
f.UserData = struct("htmlFiles", bH, "nFilesText", nF);

%wait for UserData to change before continuing (happens in getSettings function)
waitfor(f, 'UserData');
htmlFiles = f.UserData.htmlFiles; 
delete(f);
end

%% Functions
%getting target file & changing name    
function getHTMLs(src, ~)
    %getting box info
    f = ancestor(src,"figure","toplevel");
    data = f.UserData;
    %getting folder
    [hFile, hPath] = uigetfile('*.html', 'MultiSelect', 'on');
    files = struct("paths", hPath, "files", hFile);
    set(data.htmlFiles, 'UserData', files);
    %turning visibility off and onn to keep box in the foreground
    f.Visible = 'off';
    f.Visible = 'on';
    %changing number of files selected text
    nFiles = numel(files);
    if nFiles == 1 %doing this because if 1 there won't be an s and it'll bug me
        nFilesText = "1 file selected";
    else
        nFilesText = [num2str(nFiles) ' files selected'];
    end
    set(data.nFilesText, 'String', nFilesText);
end

%saving settings
function continueToExtraction(src, ~)
    %getting box info
    f = ancestor(src,"figure","toplevel");
    data = f.UserData;
    htmlFiles = data.htmlFiles.UserData;
    %changing UserData
    f.UserData = struct("htmlFiles", htmlFiles); 
end












