%% Extracting Amazon gift card codes from large email thread
% Credit: Lindsay Santacroce (contact: lindsayas22@gmail.com)
% Description: This function allows users to extract Amazon gift card codes from a large amount of emails using .html files of the email chain(s).
% Interface:% 
    % Input: .html file(s) of the email(s) containing the Amazon gift card codes
    % Output: A .csv file with all of the gift card claim codes from the email(s)
% Instructions:
    % 1. Add the function folder to MATLAB path
    % 2. Download .html files of the email(s) sent from Amazon: 
        % 2.1. Navigate to the email chain that Amazon sends containing the gift card claim codes
        % 2.2. Open the email chain in a new window and expand the email chain
        % 2.3. Click Ctrl+S to save the email chain as an .html file
        % 2.4. Amazon sends max 100 emails per chain, so repeat this step for as many email chains that contain your gift card codes 
    % 3. Type "ExtractAmazonCodes" into the MATLAB Command Window and hit Enter
    % 4. Select all the .html files you download (Ctrl+click to select more than one) and click 'Continue'
    % 5. When the function is complete, a .csv file will appear in the same folder as your .html files that will contain all Amazon gift card claim codes!

function ExtractAmazonCodes

%getting html files from box
htmlFiles = selectHTMLs;
%reading html files
giftText = "";
for h = 1:numel(htmlFiles)
    giftText = append(giftText, extractFileText([htmlFiles(h).paths htmlFiles(h).files]));
end
giftText = char(giftText);
%finding locations of the word "claim" in the emails because the codes follow that word
claimLocs = strfind(giftText, 'Claim Code');
%display number of gift card locations found (if it's not the number you're expecting, something went terribly wrong)
disp([num2str(numel(claimLocs)) ' gift card codes extracted']);
for d = 1:numel(claimLocs)
    %text of current code
    currClaim = giftText(claimLocs(d):claimLocs(d)+30);
    %positions of dashes in current claim
    dashPos = strfind(currClaim, '-');
    %claim number based on dash positions (4 characters before and 4 characters after)
    claimNum = currClaim(dashPos(1)-4:dashPos(2)+4);
    %list of codes
    amazonCodes(d) = string(claimNum);
end
%saving
amazonCodes = amazonCodes';
fileName = [htmlFiles(1).paths 'AmazonGiftCardCodes_' date() '.csv'];
writematrix(amazonCodes, fileName);
end